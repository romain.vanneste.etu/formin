'use strict';

function slowMarkedRenderer(source, target, initVisible, delay, markedOpts) {
    function delayRateLimit(delay, callback) {
        var pending = false;

        return function() {
            if(!pending) {
                pending = true;
                window.setTimeout(function() {
                    callback();
                    pending = false;
                }, delay);
            }
        };
    }

    var src = typeof source === 'string' ? document.getElementById(source) : source;
    var tgt = typeof target === 'string' ? document.getElementById(target) : target;

    var visible = initVisible;
    var renderNow = function() { tgt.innerHTML = marked(src.value, markedOpts); };
    var renderSlowly = delayRateLimit(delay, function() { renderNow() });
    var update = function() { if(visible) renderSlowly(); };

    src.addEventListener('input', function(ev) { update(); });

    return {
        update: update,
        toggle: function(v) {
                    visible = (typeof v === 'undefined') ? !visible : v;
                    if(visible) renderNow();
                }
    };
}
