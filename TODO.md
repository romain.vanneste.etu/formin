#   TODOs

-   Port to JavaScript the way PHP interprets field names so that the
    JSON import/export handle objects containing the 3 fields `key`,
    `d` and `p`; it should handle nesting, arrays, etc.
-   Provide a way to handle repeatable (sets of) form fields (for
    instance, in a form to describe a book, we would want to be able
    to add one field per author).
-   Bootstrap: define a form to describe a new form (this requires the
    previous todo). A small JavaScript function could be used to
    export the result as a working `.html`, `.php` or just HTML
    fragment.
-   Try to provide as many working features as possible in the case of
    a fully-static server, where for instance the users filling the
    form would send the data by mail: explore to which extent
    `mailto:` links can be used, with good fallbacks.
-   Add a PHP backend that just forwards the data as an email instead
    of storing it to a database (think about a contact form).
-   Improve the interactivity when filling a form by saving regularly
    the current content to local storage; maybe also allowing some
    _undos_, also with local storage.
-   Devise a simple way for users to delete their data.
-   A better stylesheet, integration with common stylesheets.
-   Define some tests. In particular, one could test whether running
    `create table if not exists`{.sql} on every submission has a
    noticeable impact on performance.
-   Make it more explicit to users that modifications in the form just
    submitted will overwrite the previous submission: add a button to
    submit a new entry?
-   Add some documentation
