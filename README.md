#   formin: A minimalist toolkit for forms

`formin` is a small toolkit to create web forms, published under the
CeCILL A 2.1 free software license. See the [license](COPYING-en) for
details.

It aims at being _minimalist_ in the following sense:

-   it should have as little requirements as possible; in particular,
    it should be using the server-side language you have available:
    this is why the first version of the backend is aimed at PHP 5.6
    with a SQLite database; and versions with purely static backends
    would be nice,

-   it should be as little as possible: less code is less bugs; as a
    consequence:

    -   the same backend code should work for all the forms on a given
        server, except for some configuration variables, so that
        creating a form should not involve fiddling with potentially
        exploitable code,

    -   the data should be exchanged between client and server in the
        native formats when possible: in the version with a PHP-SQLite
        backend, form data is sent from the client to the server using
        the standard form posting (either form-data or URL-encoding,
        since PHP offers the same interface for both), and from the
        server to the client injected in the javascript contained in
        the form,

-   it should be modular: when it makes sense, you should be able to
    use the parts of `formin` you want without requiring everything,

-   it should integrate with the rest of your stack as smoothly as
    possible: you should be able to use your standard stylesheet, tune
    the interactivity (or no interactivity) of a form as you want,
    etc.


### PHP & SQLite backend

The current version of `formin` only contains a backend in PHP (tested
with 5.6, but hopefully should run on 7 too) with a SQLite database to
store the form data.

To offer a standardized interface for all forms, the backend expects
only 3 fields:

-   `key`: a (secret) key that should be globally unique; the client
    is responsible for providing such a key; `formin.js` generates
    such a key using cryptographic primitives; the hope is that this
    `key` also serves as a simple captcha substitute (if the server
    expects at least 30 or 40 characters in that field, bots would
    probably fail to submit something unless they actually run the
    javascript),

-   `d`: a field containing all the (private) _data_; this uses the
    fact that, when fields named `d[a]` and `d[b]` are submitted, PHP
    automatically builds an object `d` containing fields `a` and `b`,

-   `p`: a field containing all the _public_ data, which can be made
    easily available to any user of the form for instance.

This simple scheme also allows to use the same database schema for any
form, with 4 columns: one for each of those fields and one for the
time of the last update.
